﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Timers;
using System.Windows.Threading;
using System.Windows.Interop;
using System.Runtime.InteropServices;


namespace AnimatedButton
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>     
    public partial class MainWindow : Window
    {
        public bool connect_state;
        
        struct KasseCurState_
        {
            public string state;
            public bool MANAGER;
            public bool BREAK;
            public bool CALLAC;
            public bool OK;

            public KasseCurState_(string state, bool nMANAGER, bool nBREAK, bool nCALLAC, bool nOK)
            {
                this.state = "unknown";
                this.MANAGER = nMANAGER;
                this.BREAK = nBREAK;
                this.OK = nOK;
                this.CALLAC = nCALLAC;
            }
        }
        struct KasseCurStateFunction_
        {
            public int numberTill;
            public bool MANAGER;
            public bool BREAK;
            public bool CALLAC;
            public bool OK;
            public bool ALARM;
            public KasseCurStateFunction_(int nnumberTill, bool nMANAGER, bool nBREAK, bool nCALLAC, bool nOK, bool nALARM)
            {
                this.numberTill = nnumberTill;
                this.MANAGER = nMANAGER;
                this.BREAK = nBREAK;
                this.OK = nOK;
                this.CALLAC = nCALLAC;
                this.ALARM = nALARM;
            }
        }
        KasseCurState_[] KasseState = new KasseCurState_[12];
        KasseCurStateFunction_ KasseCurStateFunction = new KasseCurStateFunction_();
        //string[] KasseState = new string[12];
        private int counter500ms, count1sec;

        public UdpClient client = new UdpClient();
        UdpClient receive = new UdpClient();
        //Thread threadUDP = new Thread(new ThreadStart(UDPserver));
        Button[] btns = new Button[16];
        private const int listenPort = 8888;
        Thread ReceiverThread;

        public class INIManager
        {
            //Конструктор, принимающий путь к INI-файлу
            public INIManager(string aPath)
            {
                path = aPath;
            }

            //Конструктор без аргументов (путь к INI-файлу нужно будет задать отдельно)
            public INIManager() : this("") { }

            //Возвращает значение из INI-файла (по указанным секции и ключу) 
            public string GetPrivateString(string aSection, string aKey)
            {
                //Для получения значения
                StringBuilder buffer = new StringBuilder(SIZE);

                //Получить значение в buffer
                GetPrivateString(aSection, aKey, null, buffer, SIZE, path);

                //Вернуть полученное значение
                return buffer.ToString();
            }

            //Пишет значение в INI-файл (по указанным секции и ключу) 
            public void WritePrivateString(string aSection, string aKey, string aValue)
            {
                //Записать значение в INI-файл
                WritePrivateString(aSection, aKey, aValue, path);
            }

            //Возвращает или устанавливает путь к INI файлу
            public string Path { get { return path; } set { path = value; } }

            //Поля класса
            private const int SIZE = 1024; //Максимальный размер (для чтения значения из файла)
            private string path = null; //Для хранения пути к INI-файлу

            //Импорт функции GetPrivateProfileString (для чтения значений) из библиотеки kernel32.dll
            [DllImport("kernel32.dll", EntryPoint = "GetPrivateProfileString")]
            private static extern int GetPrivateString(string section, string key, string def, StringBuilder buffer, int size, string path);

            //Импорт функции WritePrivateProfileString (для записи значений) из библиотеки kernel32.dll
            [DllImport("kernel32.dll", EntryPoint = "WritePrivateProfileString")]
            private static extern int WritePrivateString(string section, string key, string str, string path);
        }

        public MainWindow()
        {
            InitializeComponent();
            Loaded += MainWindow_Load;

            ReceiverThread = new Thread(new ThreadStart(Receiver));
            ReceiverThread.IsBackground = true;
            ReceiverThread.Start();
            Console.WriteLine("INNT FORM");

            DispatcherTimer timer1 = new DispatcherTimer();
            timer1.Tick += new EventHandler(timer1_Tick_1s);
            timer1.Interval = new TimeSpan(0, 0, 1);
            timer1.Start();

            //------------ Timer 500msec ----------------------------
            counter500ms = 0;
           // timer1.Interval = 500;
            //timer1.Enabled = true;
            // Hook up timer's tick event handler.  
            //timer1.Tick += new System.EventHandler(this.timer1_Tick_500ms);
            //------------ Timer 1000msec ----------------------------
            count1sec = 0;
           // timer1.Interval = 1;
            //timer1.Enabled = true;
            // Hook up timer's tick event handler.  
          //  timer1.Tick += new System.EventHandler(this.timer1_Tick_1s);
            for (int i = 0; i < 12; i++)
            {
                //KasseState[i] = "close";
                KasseState[i].state = "close";
                KasseState[i].BREAK = false;
                KasseState[i].MANAGER = false;
                KasseState[i].CALLAC = false;
                KasseState[i].OK = false;

            }
            KasseCurStateFunction.numberTill = int.Parse(txtBThTill.Text);
            KasseCurStateFunction.BREAK = false;
            KasseCurStateFunction.MANAGER = false;
            KasseCurStateFunction.CALLAC = false;
            KasseCurStateFunction.OK = false;


            btns[0] = BtnKasse1; btns[1] = BtnKasse2;
            btns[2] = BtnKasse3; btns[3] = BtnKasse4;
            btns[4] = BtnKasse5; btns[5] = BtnKasse6;
            btns[6] = BtnKasse7; btns[7] = BtnKasse8;

            btns[8] = btnAlarm; btns[9] = btnSnooze;
            btns[10] = btnML; btns[11] = btnMA;
            btns[12] = BtnBreak; btns[13] = BtnOk;
            btns[14] = BtnClose;
            btns[15] = BtnSave;
        }
        private void timer1_Tick_1sec(object sender, System.EventArgs e)
        {
            counter500ms = counter500ms + 1;
            /*string message1 = "500ms!" + counter500ms;
            *//*Test msg for time 500ms*//*
            if (connect_state)
            {
                byte[] data = Encoding.UTF8.GetBytes(message1);
                int numberOfSentBytes = receive.Send(data, data.Length);
            }*/
        }
        private void timer1_Tick_1s(object sender, System.EventArgs e)
        {
            Brushes color;
            for (int i = 0; i < int.Parse(txtBTill.Text); i++)
            {
                switch (KasseState[i].state)
                {
                    case "open":
                        btns[i].Background = Brushes.Green;// Color.FromArgb(0x45, 0xA1, 0x2A);
                        //BtnKasse1.Background.
                        break;
                    case "opening":
                        //color = btns[i].BackColor;
                        // Console.WriteLine("Color button  = " + color);
                        // Console.WriteLine("Color color.ToArgb()n  = " + color.ToArgb());
                        // Console.WriteLine("Color color.Namen  = " + color.Name);                        
                        if (btns[i].Background == Brushes.Orange)
                        {
                            btns[i].Background = Brushes.Red;
                        }
                        else
                        {
                            btns[i].Background = Brushes.Red;
                        }
                        break;
                    case "close":
                        btns[i].Background = Brushes.Red;
                        break;
                    case "closing":
                        
                        Console.WriteLine("Color button  = " + btns[i].Background);
                        if (btns[i].Background == Brushes.Orange)
                        {
                            btns[i].Background = Brushes.Red;
                        }
                        else
                        {
                         //   btns[i].BackColor = Color.FromArgb(0xFF, 0x00, 0x00);
                        }
                        break;
                    case "MANAGER":
                        break;
                    case "CALLAC":
                        break;
                    case "BREAK":
                        break;
                    case "OK":
                        break;
                    default:
                        break;
                }
            }
            if (KasseCurStateFunction.CALLAC)
            {
                if (btnML.Background == Brushes.Red)
                {
                    btnML.Background = Brushes.Red;
                }
                else
                    btnML.Background = Brushes.Purple;
            }
            else
                btnML.Background = Brushes.Purple;

            
            if (KasseCurStateFunction.MANAGER)
            {
                if (btnMA.Background == Brushes.Orange)
                {
                    btnMA.Background = Brushes.White;
                }
                else
                    btnMA.Background = Brushes.Orange;
            }
            else
                btnMA.Background = Brushes.Orange;

            if (KasseCurStateFunction.ALARM)
            {
                if (btnAlarm.Background == Brushes.White)
                {
                    btnAlarm.Background = Brushes.Red;
                }
                else
                    btnAlarm.Background = Brushes.White;
            }
            else
                btnAlarm.Background = Brushes.White;

            count1sec++;

            if (count1sec == 5)
            {
                Send_Get_State();
                count1sec = 0;
            }

        }



        public void Receiver()
        {
            bool done = true;

            int ainState = 0;
            //UdpClient receive = new UdpClient();            

            while (true)
            {

                UdpClient listener = new UdpClient(listenPort);
                IPEndPoint groupEP = new IPEndPoint(IPAddress.Any, listenPort);
                string received_data;
                byte[] receive_byte_array;
                try
                {
                    while (done)
                    {

                        string[] status_kasse = { "open", "opening", "close", "closing", "CALLAC", "MANAGER", "BREAK", "OK", "ALARM" };
                        string[] delimiterChars = { "/" };
                        string status;
                        /* temp state Function button for next update*/
                        bool rec_MANAGER = false;
                        bool rec_BREAK = false;
                        bool rec_OK = false;
                        bool rec_CALLAC = false;
                        bool rec_ALARM = false;

                        receive_byte_array = listener.Receive(ref groupEP);
                        received_data = Encoding.ASCII.GetString(receive_byte_array, 0, receive_byte_array.Length);
                        string[] words = received_data.Split(delimiterChars, System.StringSplitOptions.RemoveEmptyEntries);

                        foreach (var word in words)
                        {
                            string tmp_data;
                            tmp_data = word;
                            switch (tmp_data)
                            {
                                case "till":
                                    System.Console.WriteLine($"\tfind till");
                                    break;

                                default:
                                    if (word.Length != 0)
                                    {
                                        if (int.TryParse(word.Substring(0, 2), out ainState))
                                        {
                                            status = word.Substring(2);
                                            System.Console.WriteLine($"number-<{ainState}> status-<{status}>");
                                            switch (status)
                                            {
                                                case "open":
                                                    btns[ainState - 1].Background = Brushes.Green;// FromArgb(0x45, 0xA1, 0x2A);
                                                    KasseState[ainState - 1].state = status;
                                                    break;
                                                case "opening":
                                                    btns[ainState - 1].Background = Brushes.Green;//FromArgb(0x45, 0xA1, 0x2A);
                                                    KasseState[ainState - 1].state = status;
                                                    break;
                                                case "close":
                                                    btns[ainState - 1].Background = Brushes.Red;//FromArgb(255, 0, 0);
                                                    KasseState[ainState - 1].state = status;
                                                    break;
                                                case "closing":
                                                    btns[ainState - 1].Background = Brushes.Red;// FromArgb(255, 0, 0);
                                                    KasseState[ainState - 1].state = status;
                                                    break;
                                                case "CALLAC":
                                                    /*When you get xxCALLAC in second row the lila button starts flashing.Look to screeny.
                                                    * When somebody push this Button you send / till / xx / set / CALLAC*/
                                                    KasseState[ainState - 1].CALLAC = true;
                                                    //KasseState[ainState - 1].state = status;                                                    
                                                    rec_CALLAC = true;
                                                    break;
                                                case "MANAGER":
                                                    /*When you get in answear xxMANAGER orange Button in second starts flashing. 
                                                     * When somebody push this Button you send /till/xx/set/MANAGER */
                                                    KasseState[ainState - 1].MANAGER = true;
                                                    rec_MANAGER = true;
                                                    break;
                                                case "BREAK":
                                                    /*When you get xxbreak you have to make the button in first row Brown. 
                                                     * xx is till (button) number. When somebody push this button you have to send /till/xx/set/break.*/
                                                    //KasseState[ainState - 1] = status;
                                                    KasseState[ainState - 1].BREAK = true;
                                                    btns[ainState - 1].Background = Brushes.Red;//FromArgb(0x96, 0x4B, 0x00);
                                                    KasseCurStateFunction.BREAK = true;
                                                    rec_BREAK = true;
                                                    break;
                                                case "OK":
                                                    KasseState[ainState - 1].OK = true;
                                                    /*OK Button: When somebody push ok and this till is closed you send / till / xx / set / open
                                                     If this till status is open you send /till/xx/set/closed
                                                     */
                                                    KasseCurStateFunction.OK = true;
                                                    rec_OK = true;
                                                    break;
                                                case "ALARM":
                                                    rec_ALARM = true;
                                                    break;
                                            }
                                            KasseCurStateFunction.MANAGER = rec_MANAGER;
                                            KasseCurStateFunction.OK = rec_OK;
                                            KasseCurStateFunction.BREAK = rec_BREAK;
                                            KasseCurStateFunction.CALLAC = rec_CALLAC;
                                            KasseCurStateFunction.ALARM = rec_ALARM;
                                        }
                                    }
                                    break;
                            }
                            if (word.Length != 0)
                            {

                                System.Console.WriteLine($"<{word}>");

                            }
                        }
                        Send_Get_State();
                        Console.WriteLine(received_data);
                    }

                }
                catch (Exception ee)
                {
                    Console.WriteLine(ee.ToString());
                }
                listener.Close();

                Thread.Sleep(1000);
            }
        }
  
        private void createConnect()
        {
            client.Connect(txtBIP.Text, int.Parse(txtBPort.Text));
            connect_state = true;
        }
        private void MainWindow_Load(object sender, RoutedEventArgs e)
        {

            client.Connect(txtBIP.Text, int.Parse(txtBPort.Text));


            /*read NET config*/
            string path = System.IO.Path.GetDirectoryName(System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName) + "\\net.ini";
           // string path = Application.StartupPath + "\\net.ini"; // возвращает текущую
                                                                 //дирректорию запущенного файла и добавляет к ней папку и имя файла.
            string s = "text"; // эта переменная обязательно должна быть не пустой.
                               // нельзя задать её просто string s; Почему? понятия не имею, но если переменная изначально пустая,
                               // функция фозвращает длину ключа а не его значение.
            INIManager inimanager = new INIManager(path);
            Console.WriteLine("INI file path  = " + path);
            //Получить значение по ключу name из секции main
            txtBIP.Text = inimanager.GetPrivateString("net", "IP");
            txtBPort.Text = inimanager.GetPrivateString("net", "Port");
            txtBTill.Text = inimanager.GetPrivateString("net", "Tills");
            txtBThTill.Text = inimanager.GetPrivateString("net", "NTill");

            if (txtBIP.Text == "")
            {
                inimanager.WritePrivateString("net", "IP", "192.168.0.100");
                txtBIP.Text = "192.168.0.100";
            }
            if (txtBPort.Text == "")
            {
                inimanager.WritePrivateString("net", "Port", "8888");
                txtBPort.Text = "8888";
            }
            if (txtBTill.Text == "")
            {
                inimanager.WritePrivateString("net", "Tills", "8");
                txtBTill.Text = "8";
            }
            if (txtBThTill.Text == "")
            {
                inimanager.WritePrivateString("net", "NTill", "3");
                txtBThTill.Text = "3";
            }
            /*create UDP connect*/

            string message = "Second Thread!";
            byte[] data = Encoding.UTF8.GetBytes(message);
            try
            {
                client.Connect(txtBIP.Text, int.Parse(txtBPort.Text));
                connect_state = true;

            }
            catch (SocketException ersend)
            {
                if (ersend.ErrorCode == 10061)
                    Console.WriteLine("Port is closed");
                else if (ersend.ErrorCode == 10060)
                    Console.WriteLine("TimeOut");
                else Console.WriteLine(ersend.Message);
            }
            int numberOfSentBytes = client.Send(data, data.Length);
        }

        private void ConfigurationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            groupBox1.Visibility = Visibility.Hidden;
            grBoxManager.Visibility = Visibility.Visible;
        }
        private void ManagerviewToolStripMenuItem_Click(object sender, EventArgs e)
        {            
            groupBox1.Visibility = Visibility.Visible;
            grBoxManager.Visibility = Visibility.Hidden;
        }

        public string Send_state_Kasse(int numberKasse)
        {
            string answer = "unknown";
            string number = "0" + numberKasse;
            numberKasse -= 1;
            if (connect_state)
            {
                /* KasseState.KsAr[numberKasse] - its current state  */
                switch (KasseState[numberKasse].state)
                {
                    case "open":
                        answer = "till/" + number + "/set/close";
                        KasseState[numberKasse].state = "close";
                        break;
                    case "opening":
                        answer = "till/" + number + "/set/open";
                        KasseState[numberKasse].state = "open";
                        break;
                    case "close":
                        answer = "till/" + number + "/set/open";
                        KasseState[numberKasse].state = "open";
                        break;
                    case "closing":
                        answer = "till/" + number + "/set/close";
                        KasseState[numberKasse].state = "close";
                        break;
                    case "BREAK":
                        answer = "till/" + number + "/set/BREAK";
                        KasseState[numberKasse].state = "BREAK";
                        break;
                    case "MANAGER":
                        answer = "till/" + number + "/set/MANAGER";
                        break;
                    case "CALLAC":
                        answer = "till/" + number + "/set/CALLAC";
                        break;
                    default:
                        break;
                }


                byte[] data = Encoding.UTF8.GetBytes(answer);
                int numberOfSentBytes = client.Send(data, data.Length);

                for (int i = 0; i < int.Parse(txtBTill.Text); i++)
                {

                }
            }
            else
                MessageBox.Show("Could not create connection.", "Error Connection", MessageBoxButton.OK, MessageBoxImage.Warning);
            // MessageBox.Show("Could not create connection.", "Error Connection", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            return answer;
        }
        private void BtnKasse1_Click(object sender, EventArgs e)
        {
            Send_state_Kasse(1);
        }
        private void BtnKasse2_Click(object sender, EventArgs e)
        {
            Send_state_Kasse(2);
        }
        private void BtnKasse3_Click(object sender, EventArgs e)
        {
            Send_state_Kasse(3);
        }
        private void BtnKasse4_Click(object sender, EventArgs e)
        {
            Send_state_Kasse(4);
        }
        private void BtnKasse5_Click(object sender, EventArgs e)
        {
            Send_state_Kasse(5);
        }
        private void BtnKasse6_Click(object sender, EventArgs e)
        {
            Send_state_Kasse(6);
        }
        private void BtnKasse7_Click(object sender, EventArgs e)
        {
            Send_state_Kasse(7);
        }
        private void BtnKasse8_Click(object sender, EventArgs e)
        {
            Send_state_Kasse(8);
        }

        private void defaultInstance_WindowClosed(object sender, EventArgs e)
        {
            client.Close();
        }
        public string Send_Get_State()
        {
            string answer = "unknown";

            answer = "till/#";

            if (connect_state)
            {
                byte[] data = Encoding.UTF8.GetBytes(answer);
                int numberOfSentBytes = client.Send(data, data.Length);

                for (int i = 0; i < int.Parse(txtBTill.Text); i++)
                {

                }
            }

            return answer;
        }
        public string Send_option_Kasse(int numberTill, string state)
        {
            string answer = "unknown";
            string number = "0" + numberTill;

            answer = "till/" + number + "/set/" + state;

            if (connect_state)
            {
                byte[] data = Encoding.UTF8.GetBytes(answer);
                int numberOfSentBytes = client.Send(data, data.Length);

                for (int i = 0; i < int.Parse(txtBTill.Text); i++)
                {

                }
            }

            return answer;
        }
        private void BtnOk_Click(object sender, EventArgs e)
        {
            Send_option_Kasse(int.Parse(txtBThTill.Text), "OK");
        }

        private void BtnBreak_Click(object sender, EventArgs e)
        {
            Send_option_Kasse(int.Parse(txtBThTill.Text), "BREAK");
        }

        private void BtnML_Click(object sender, EventArgs e)/*CALLAC*/
        {
            Send_option_Kasse(int.Parse(txtBThTill.Text), "CALLAC");
            KasseCurStateFunction.CALLAC = false;
        }

        private void BtnMA_Click(object sender, EventArgs e)/*MANAGER*/
        {
            Send_option_Kasse(int.Parse(txtBThTill.Text), "MANAGER");
            KasseCurStateFunction.MANAGER = false;
        }

        private void BtnSnooze_Click(object sender, EventArgs e)
        {
            Send_option_Kasse(int.Parse(txtBThTill.Text), "snooze");
            KasseCurStateFunction.ALARM = false;
        }

        private void BtnAlarm_Click(object sender, EventArgs e)
        {

        }

        private void BtnClose_Click(object sender, EventArgs e)
        {
            connect_state = false;
            client.Close();
        }
        static bool CheckIp(string address) //check for correct IP addres
        {
            var nums = address.Split('.');
            int useless;
            return nums.Length == 4 && nums.All(n => int.TryParse(n, out useless)) &&
               nums.Select(int.Parse).All(n => n < 256);
        }
        private void TxtBIP_KeyPress(object sender, TextCompositionEventArgs e)
        {
            if (e.Text == ".")
            {
                 if (((TextBox)sender).Text.IndexOf(e.Text) > -1)                
                {
                    e.Handled = true;
                }
            }
            else if (IsNumber(e.Text) == false)
            {
                e.Handled = true;
            }
            /*          if (!char.IsNumber(e.Text, e.Text.Length - 1))
                      {
                          e.Handled = true;
                      }

                      if (((e.Text).ToCharArray()[e.Text.Length - 1] == '.') || ((e.Text).ToCharArray()[e.Text.Length - 1] == ','))
                      {
                          e.Handled = true;
                          if (!(((TextBox)sender).Text.Contains('.')))
                          {
                              if (((TextBox)sender).Text.Length == 0) { ((TextBox)sender).Text = "0."; ((TextBox)sender).CaretIndex = ((TextBox)sender).Text.Length; }
                              else { ((TextBox)sender).Text += "."; ((TextBox)sender).CaretIndex = ((TextBox)sender).Text.Length; }
                          }
                      }
                      if ((e.Text).ToCharArray()[e.Text.Length - 1] == '-' & !((TextBox)sender).Text.Contains('-')) { e.Handled = true; ((TextBox)sender).Text = "-" + ((TextBox)sender).Text; ((TextBox)sender).CaretIndex = ((TextBox)sender).Text.Length; }
                      if ((e.Text).ToCharArray()[e.Text.Length - 1] == '+' & ((TextBox)sender).Text.Contains('-')) { e.Handled = true; ((TextBox)sender).Text = ((TextBox)sender).Text.Substring(1); ((TextBox)sender).CaretIndex = ((TextBox)sender).Text.Length; }
                  */
        }
            private bool IsNumber(string Text)
            {
                int output;
                return int.TryParse(Text, out output);
            }
        
    

        private void TxtBPort_KeyPress(object sender, TextCompositionEventArgs e)
        {
            if (IsNumber(e.Text) == false)   // check for enter only numeral
                e.Handled = true;
        }

        private void TxtBTill_KeyPress(object sender, TextCompositionEventArgs e)
        {
            if (IsNumber(e.Text) == false)   // check for enter only numeral
                e.Handled = true;
        }

        private void TxtBThTill_KeyPress(object sender, TextCompositionEventArgs e)
        {
            if (IsNumber(e.Text) == false)   // check for enter only numeral
                e.Handled = true;
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            string path = System.IO.Path.GetDirectoryName(System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName) + "\\net.ini";
            //string path = Application.StartupPath + "\\net.ini"; // возвращает текущую
                                                                 //дирректорию запущенного файла и добавляет к ней папку и имя файла.
            string s = "text";
            INIManager inimanager = new INIManager(path);


            if (CheckIp(txtBIP.Text))
            {
                inimanager.WritePrivateString("net", "IP", txtBIP.Text);
                inimanager.WritePrivateString("net", "Port", txtBPort.Text);
                inimanager.WritePrivateString("net", "Tills", txtBTill.Text);
                inimanager.WritePrivateString("net", "NTill", txtBThTill.Text);

                //connect_state = false;
                //client.Close();
                //createConnect();
            }
            else
                //MessageBox.Show("Wrong IP", "NET Param", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            MessageBox.Show("Wrong IP", "NET Param", MessageBoxButton.OK, MessageBoxImage.Warning);


        }
        private void TxtBPort_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void BtnKasse6_Click_1(object sender, RoutedEventArgs e)
        {

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
