﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Timers;
using System.Windows.Threading;
using System.Windows.Interop;
using System.Runtime.InteropServices;


namespace AnimatedButton
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>     
    public partial class MainWindow : Window
    {
        public bool connect_state;
        public int numberway;
        struct KasseCurState_
        {
            public string state;
            public int countBlink;
            public bool stateblink;
            public bool MANAGER;
            public bool BREAK;
            public bool CALLAC;
            public bool OK;

            public KasseCurState_(string state, int ncountBlink, bool nstateblink, bool nMANAGER, bool nBREAK, bool nCALLAC, bool nOK)
            {
                this.state = "unknown";
                this.countBlink = ncountBlink;
                this.stateblink = nstateblink;
                this.MANAGER = nMANAGER;
                this.BREAK = nBREAK;
                this.OK = nOK;
                this.CALLAC = nCALLAC;
            }
        }
        struct KasseCurStateFunction_
        {
            public int numberTill;

            public bool MANAGER;
            public bool BREAK;
            public bool CALLAC;
            public bool OK;
            public bool ALARM;
            public KasseCurStateFunction_(int nnumberTill,  bool nMANAGER, bool nBREAK, bool nCALLAC, bool nOK, bool nALARM)
            {
                this.numberTill = nnumberTill;

                this.MANAGER = nMANAGER;
                this.BREAK = nBREAK;
                this.OK = nOK;
                this.CALLAC = nCALLAC;
                this.ALARM = nALARM;
            }
        }
        KasseCurState_[] KasseState = new KasseCurState_[12];
        KasseCurStateFunction_ KasseCurStateFunction = new KasseCurStateFunction_();
        //string[] KasseState = new string[12];
        private int counter500ms, count1sec;

        public UdpClient client = new UdpClient();
        UdpClient receive = new UdpClient();
        
        Button[] btns = new Button[16];
        private int listenPort = 8888;
        private int currentThill;
        private string IPAddrtxtbox;
        Thread ReceiverThread;

        public class INIManager
        {
            //Конструктор, принимающий путь к INI-файлу
            public INIManager(string aPath)
            {
                path = aPath;
            }

            //Конструктор без аргументов (путь к INI-файлу нужно будет задать отдельно)
            public INIManager() : this("") { }

            //Возвращает значение из INI-файла (по указанным секции и ключу) 
            public string GetPrivateString(string aSection, string aKey)
            {
                //Для получения значения
                StringBuilder buffer = new StringBuilder(SIZE);

                //Получить значение в buffer
                GetPrivateString(aSection, aKey, null, buffer, SIZE, path);

                //Вернуть полученное значение
                return buffer.ToString();
            }

            //Пишет значение в INI-файл (по указанным секции и ключу) 
            public void WritePrivateString(string aSection, string aKey, string aValue)
            {
                //Записать значение в INI-файл
                WritePrivateString(aSection, aKey, aValue, path);
            }

            //Возвращает или устанавливает путь к INI файлу
            public string Path { get { return path; } set { path = value; } }

            //Поля класса
            private const int SIZE = 1024; //Максимальный размер (для чтения значения из файла)
            private string path = null; //Для хранения пути к INI-файлу

            //Импорт функции GetPrivateProfileString (для чтения значений) из библиотеки kernel32.dll
            [DllImport("kernel32.dll", EntryPoint = "GetPrivateProfileString")]
            private static extern int GetPrivateString(string section, string key, string def, StringBuilder buffer, int size, string path);

            //Импорт функции WritePrivateProfileString (для записи значений) из библиотеки kernel32.dll
            [DllImport("kernel32.dll", EntryPoint = "WritePrivateProfileString")]
            private static extern int WritePrivateString(string section, string key, string str, string path);
        }

        public MainWindow()
        {
            InitializeComponent();
            Loaded += MainWindow_Load;

            Console.WriteLine("INNT FORM");

            DispatcherTimer timer1 = new DispatcherTimer();
            timer1.Tick += new EventHandler(timer1_Tick_1s);
            timer1.Interval = TimeSpan.FromMilliseconds(1000);
            timer1.Start();

            DispatcherTimer timerStateKasse = new DispatcherTimer();
            timerStateKasse.Tick += new EventHandler(timerStateKasse_Tick_100ms);
            timerStateKasse.Interval = TimeSpan.FromMilliseconds(100);

            //------------ Timer 500msec ----------------------------
            counter500ms = 0;         
            count1sec = 0;         
            for (int i = 0; i < 12; i++)
            {
                //KasseState[i] = "close";
                KasseState[i].state = "close";
                KasseState[i].BREAK = false;
                KasseState[i].MANAGER = false;
                KasseState[i].CALLAC = false;
                KasseState[i].OK = false;

            }
            KasseCurStateFunction.numberTill = int.Parse(txtBThTill.Text);
            KasseCurStateFunction.BREAK = false;
            KasseCurStateFunction.MANAGER = false;
            KasseCurStateFunction.CALLAC = false;
            KasseCurStateFunction.OK = false;


            btns[0] = BtnKasse1; btns[1] = BtnKasse2;
            btns[2] = BtnKasse3; btns[3] = BtnKasse4;
            btns[4] = BtnKasse5; btns[5] = BtnKasse6;
            btns[6] = BtnKasse7; btns[7] = BtnKasse8;

            for (int i = 0; i < 8; i++)
                btns[i].Visibility = Visibility.Hidden;

            for (int i = 0; i < int.Parse(txtBTill.Text); i++)
                btns[i].Visibility = Visibility.Visible;


            btns[8] = btnAlarm; btns[9] = btnSnooze;
            btns[10] = btnML; btns[11] = btnMA;
            btns[12] = BtnBreak; btns[13] = BtnOk;
            btns[14] = BtnClose;
            btns[15] = BtnSave;
            
        }
        private void timer1_Tick_1sec(object sender, System.EventArgs e)
        {
            counter500ms = counter500ms + 1;
            /*string message1 = "500ms!" + counter500ms;
            *//*Test msg for time 500ms*//*
            if (connect_state)
            {
                byte[] data = Encoding.UTF8.GetBytes(message1);
                int numberOfSentBytes = receive.Send(data, data.Length);
            }*/
        }
        private void timerStateKasse_Tick_100ms(object sender, System.EventArgs e)
        {            
            
        }
        private void timer1_Tick_1s(object sender, System.EventArgs e)
        {            
            for (int i = 0; i < int.Parse(txtBTill.Text); i++)
            {

                switch (KasseState[i].state)
                {
                    case "open":
                    case "OPEN":
                        if (((int.Parse(txtBThTill.Text) - 1) == i) && (KasseCurStateFunction.BREAK == true))
                        {

                        } else
                            btns[i].Background = Brushes.Green;
                        break;
                    case "opening":
                    case "OPENING":
                        if (((int.Parse(txtBThTill.Text) - 1) == i) && (KasseCurStateFunction.BREAK == true))
                        {

                        }
                        else
                        {
                            if (btns[i].Background == Brushes.White)
                            {
                                btns[i].Background = Brushes.Green;
                                btns[i].Foreground = Brushes.White;
                            }
                            else
                            {
                                btns[i].Background = Brushes.White;
                                btns[i].Foreground = Brushes.Black;
                            }
                        }
                            break;
                        
                        case "close":
                        case "CLOSE":
                        if (((int.Parse(txtBThTill.Text) - 1) == i) && (KasseCurStateFunction.BREAK == true))
                        {

                        }
                        else
                        {
                            btns[i].Background = Brushes.Red;
                            btns[i].Foreground = Brushes.White;//fix black Foreground
                        }
                            break;
                        
                        case "closing":
                        case "CLOSING":
                        if (((int.Parse(txtBThTill.Text) - 1) == i) && (KasseCurStateFunction.BREAK == true))
                        {

                        }
                        else
                        {
                            if (btns[i].Background == Brushes.White)
                            {
                                btns[i].Background = Brushes.Red;
                                btns[i].Foreground = Brushes.White;
                            }
                            else
                            {
                                btns[i].Background = Brushes.White;
                                btns[i].Foreground = Brushes.Black;
                            }
                        }
                            break;
                        case "MANAGER":
                            break;
                        case "CALLAC":
                            break;
                        case "BREAK":
                            break;
                        case "OK":
                            break;
                        default:
                            break;
                    }
                
            }
            if (KasseCurStateFunction.CALLAC)
            {
                if (btnML.Background == Brushes.Gray)
                {
                    btnML.Background = Brushes.White;
                    btnML.Foreground = Brushes.Black;
                }
                else
                {
                    btnML.Background = Brushes.Gray;
                    btnML.Foreground = Brushes.White;
                }
            }
            else
                btnML.Background = Brushes.Gray;

            
            if (KasseCurStateFunction.MANAGER)
            {
                if (btnMA.Background == Brushes.Gray)
                {
                    btnMA.Background = Brushes.White;
                    btnMA.Foreground = Brushes.Black;
                }
                else
                {
                    btnMA.Background = Brushes.Gray;
                    btnMA.Foreground = Brushes.White;
                }
            }
            else
                btnMA.Background = Brushes.Gray;

            if (KasseCurStateFunction.ALARM)
            {
                if (btnAlarm.Background == Brushes.Gray)
                {
                    btnAlarm.Background = Brushes.Red;
                }
                else
                    btnAlarm.Background = Brushes.Gray;
            }
            else
                btnAlarm.Background = Brushes.Gray;

            if (KasseCurStateFunction.BREAK)
            {
                if((btns[int.Parse(txtBThTill.Text)-1].Background == Brushes.Green)||(btns[int.Parse(txtBThTill.Text)-1].Background == Brushes.Red))
               {
                    btns[int.Parse(txtBThTill.Text)-1].Background = Brushes.White;
                    btns[int.Parse(txtBThTill.Text) - 1].Foreground = Brushes.Black;
                }
                else
                {
                    btns[int.Parse(txtBThTill.Text)-1].Background = Brushes.Red;
                    btns[int.Parse(txtBThTill.Text) - 1].Foreground = Brushes.White;
                }

                BtnBreak.Background = Brushes.Red;
            }
            else
                BtnBreak.Background = Brushes.Gray;

            count1sec++;
            
            if (count1sec == 5)
            {
                /* get update state */
                Send_Get_State();
                count1sec = 0;
            }
        }



        public void Receiver()
        {
            bool done = true;            
            int ainState = 0;
            int numberThill_get;


            while (true)
            {
                UdpClient listener = new UdpClient(listenPort);
                IPEndPoint groupEP = new IPEndPoint(IPAddress.Parse(IPAddrtxtbox)/*IPAddress.Any*/, listenPort);
                string received_data;
                byte[] receive_byte_array;
                try
                {
                    while (done)
                    {
                        string[] status_kasse = { "open", "opening", "close", "closing", "CALLAC", "MANAGER", "BREAK", "OK", "ALARM", "manager", "break", "ok", "alarm", "callac", };
                        string[] delimiterChars = { "/","$" };
                        string status;
                        /* temp state Function button for next update*/
                        bool rec_MANAGER = false;
                        bool rec_BREAK = false;
                        bool rec_OK = false;
                        bool rec_CALLAC = false;
                        bool rec_ALARM = false;

                        receive_byte_array = listener.Receive(ref groupEP);
                        received_data = Encoding.ASCII.GetString(receive_byte_array, 0, receive_byte_array.Length);
                        string[] words = received_data.Split(delimiterChars, System.StringSplitOptions.RemoveEmptyEntries);
                        System.Console.WriteLine($" Receive state-<{received_data}>");
                        
                        foreach (var word in words)
                        {

                            string tmp_data;
                            tmp_data = word;
                           
                            switch (tmp_data)
                            {
                                case "till":
                                    
                                    break;

                                default:
                                    if (word.Length != 0)
                                    {

                                        if (int.TryParse(word.Substring(0, 2), out ainState))
                                        {
                                            int.TryParse(word.Substring(0, 2), out numberThill_get);
                                            status = word.Substring(2);
                                            //System.Console.WriteLine($"number-<{ainState}> status-<{status}>");
                                            switch (status)
                                            {
                                                case "open":
                                                case "OPEN":
                                                    //btns[ainState - 1].Background = Brushes.Green;// FromArgb(0x45, 0xA1, 0x2A);
                                                    KasseState[ainState - 1].state = status;
                                                    KasseState[ainState - 1].stateblink = true;
                                                    break;
                                                case "opening":
                                                case "OPENING":
                                                    //btns[ainState - 1].Background = Brushes.Green;//FromArgb(0x45, 0xA1, 0x2A);
                                                    KasseState[ainState - 1].state = status;
                                                    KasseState[ainState - 1].stateblink = true;
                                                    break;
                                                case "close":
                                                case "CLOSE":
                                                    //btns[ainState - 1].Background = Brushes.Red;//FromArgb(255, 0, 0);
                                                    KasseState[ainState - 1].state = status;
                                                    KasseState[ainState - 1].stateblink = true;
                                                    break;
                                                case "closing":
                                                case "CLOSING":
                                                    //btns[ainState - 1].Background = Brushes.Red;// FromArgb(255, 0, 0);
                                                    KasseState[ainState - 1].state = status;
                                                    KasseState[ainState - 1].stateblink = true;
                                                    break;
                                                case "CALLAC":
                                                case "callac":
                                                    /*When you get xxCALLAC in second row the lila button starts flashing.Look to screeny.
                                                    * When somebody push this Button you send / till / xx / set / CALLAC*/
                                                    if (currentThill == ainState)
                                                    {
                                                        KasseState[ainState - 1].CALLAC = true;
                                                    }
                                                    //KasseState[ainState - 1].state = status;                                                    
                                                    rec_CALLAC = true;
                                                    break;
                                                case "MANAGER":
                                                case "manager":
                                                    /*When you get in answear xxMANAGER orange Button in second starts flashing. 
                                                     * When somebody push this Button you send /till/xx/set/MANAGER */
                                                    if (currentThill == ainState)
                                                    {
                                                        KasseState[ainState - 1].MANAGER = true;
                                                        rec_MANAGER = true;
                                                    }
                                                    break;
                                                case "BREAK":
                                                case "break":
                                                    if (currentThill == ainState)
                                                    {
                                                        /*When you get xxbreak you have to make the button in first row Brown. 
                                                         * xx is till (button) number. When somebody push this button you have to send /till/xx/set/break.*/
                                                        //KasseState[ainState - 1] = status;
                                                        KasseState[ainState - 1].BREAK = true;
                                                        //btns[ainState - 1].Background = Brushes.Red;//FromArgb(0x96, 0x4B, 0x00);
                                                        KasseCurStateFunction.BREAK = true;
                                                        rec_BREAK = true;
                                                    }
                                                    break;
                                                case "OK":
                                                case "ok":
                                                    if (currentThill == numberThill_get)
                                                    {
                                                        KasseState[ainState - 1].OK = true;
                                                        /*OK Button: When somebody push ok and this till is closed you send / till / xx / set / open
                                                        If this till status is open you send /till/xx/set/closed
                                                        */
                                                        KasseCurStateFunction.OK = true;
                                                    }
                                                    rec_OK = true;
                                                    break;
                                                case "QALARM":
                                                case "qalarm":
                                                    if (currentThill == numberThill_get)
                                                    {
                                                        rec_ALARM = true;
                                                    }
                                                    break;
                                            }
                                         /*   KasseCurStateFunction.MANAGER = rec_MANAGER;
                                            KasseCurStateFunction.OK = rec_OK;
                                            KasseCurStateFunction.BREAK = rec_BREAK;
                                            KasseCurStateFunction.CALLAC = rec_CALLAC;
                                            KasseCurStateFunction.ALARM = rec_ALARM;*/
                                        }
                                        KasseCurStateFunction.MANAGER = rec_MANAGER;
                                        KasseCurStateFunction.OK = rec_OK;
                                        KasseCurStateFunction.BREAK = rec_BREAK;
                                        KasseCurStateFunction.CALLAC = rec_CALLAC;
                                        KasseCurStateFunction.ALARM = rec_ALARM;
                                    }
                                    break;
                            }
                            if (word.Length != 0)
                            {

                               // System.Console.WriteLine($"<{word}>");

                            }
                        }
                        //Send_Get_State();
                        //Console.WriteLine(received_data);
                    }

                }
                catch (Exception ee)
                {
                    //Console.WriteLine(ee.ToString());
                }
                listener.Close();

                Thread.Sleep(1000);
            }
        }
  
        private void createConnect()
        {
            client.Connect(txtBIP.Text, int.Parse(txtBPort.Text));
            connect_state = true;
        }
        private void MainWindow_Load(object sender, RoutedEventArgs e)
        {
            client.Connect(txtBIP.Text, int.Parse(txtBPort.Text));
            groupBoxConfig.Visibility = Visibility.Hidden;
            DockPanelManager.Visibility = Visibility.Visible;

            /*read NET config*/
            string path = System.IO.Path.GetDirectoryName(System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName) + "\\net.ini";           
            string s = "text"; // эта переменная обязательно должна быть не пустой.
                               // нельзя задать её просто string s; Почему? понятия не имею, но если переменная изначально пустая,
                               // функция фозвращает длину ключа а не его значение.
            INIManager inimanager = new INIManager(path);
            //Console.WriteLine("INI file path  = " + path);
            //Получить значение по ключу name из секции main
            txtBIP.Text = inimanager.GetPrivateString("net", "IP");
            txtBPort.Text = inimanager.GetPrivateString("net", "Port");
            txtBTill.Text = inimanager.GetPrivateString("net", "Tills");
            txtBThTill.Text = inimanager.GetPrivateString("net", "NTill");
            txtBPort_Listen.Text = inimanager.GetPrivateString("net", "PortList");
            IPAddrtxtbox = txtBIP.Text;
            s = inimanager.GetPrivateString("net", "way");
            if(s == "Short")
                cmbBoxWay.SelectedIndex = 1;
            else if (s == "Long")
                cmbBoxWay.SelectedIndex = 2;

            if (txtBIP.Text == "")
            {
                inimanager.WritePrivateString("net", "IP", "192.168.0.100");
                txtBIP.Text = "192.168.0.100";
            }
            if (txtBPort.Text == "")
            {
                inimanager.WritePrivateString("net", "Port", "8888");
                txtBPort.Text = "8888";
            }
            if (txtBPort_Listen.Text == "")
            {
                inimanager.WritePrivateString("net", "PortList", "8887");
                txtBPort.Text = "8887";
            }
            if (txtBTill.Text == "")
            {
                inimanager.WritePrivateString("net", "Tills", "8");
                txtBTill.Text = "8";
            }
            if (txtBThTill.Text == "")
            {
                inimanager.WritePrivateString("net", "NTill", "3");
                txtBThTill.Text = "3";
            }
            currentThill = int.Parse(txtBThTill.Text);
            ReceiverThread = new Thread(new ThreadStart(Receiver));
            ReceiverThread.IsBackground = true;
            ReceiverThread.Start();
            listenPort = Convert.ToInt32(txtBPort_Listen.Text);

            /*create UDP connect*/

            string message = "/till/#";
            byte[] data = Encoding.UTF8.GetBytes(message);
            try
            {
                client.Connect(txtBIP.Text, int.Parse(txtBPort.Text));
                connect_state = true;

            }
            catch (SocketException ersend)
            {
                /*if (ersend.ErrorCode == 10061)
                    Console.WriteLine("Port is closed");
                else if (ersend.ErrorCode == 10060)
                    Console.WriteLine("TimeOut");
                else Console.WriteLine(ersend.Message);*/
            }
            int numberOfSentBytes = client.Send(data, data.Length);


            for (int i = 0; i < 8; i++)
                btns[i].Visibility = Visibility.Hidden;

            for (int i = 0; i < Convert.ToInt32(txtBTill.Text); i++)
                btns[i].Visibility = Visibility.Visible;
        }

        private void ConfigurationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < 8; i++)
                btns[i].Visibility = Visibility.Hidden;

            for (int i = 0; i < Convert.ToInt32(txtBTill.Text); i++)
                btns[i].Visibility = Visibility.Visible;

            numberway = cmbBoxWay.SelectedIndex;
            if (numberway == 1)/*short way no. Not hidden. Only the letters ok should be away*/
                BtnOk.Content = "";
            else
                BtnOk.Content = "OK";
            groupBoxConfig.Visibility = Visibility.Hidden;
            DockPanelManager.Visibility = Visibility.Visible;

            currentThill = int.Parse(txtBThTill.Text);
        }
        private void ManagerviewToolStripMenuItem_Click(object sender, EventArgs e)
        {
            groupBoxConfig.Visibility = Visibility.Visible;
            DockPanelManager.Visibility = Visibility.Hidden;

            currentThill = int.Parse(txtBThTill.Text);
        }
        public string Send_OK(int numberKasse)
        {
            string answer = "unknown";
            string number = "0" + numberKasse;
            numberKasse -= 1;
            numberway = cmbBoxWay.SelectedIndex;
            if (connect_state)
            {
                /* KasseState.KsAr[numberKasse] - its current state  
                 *  numberway :
                 * 1 = short
                 * 2 = long
                 * **/
                switch (KasseState[numberKasse].state)
                {
                    case "open":
                    case "OPEN":
                        if (numberway == 1)
                        {
                        }
                        else
                        {
                            answer = "/till/" + number + "/set/close";
                            KasseState[numberKasse].state = "close";
                        }
                        break;
                    case "opening":
                    case "OPENING":
                        if (numberway == 1)
                        {
                            
                        }
                        else
                        {
                            answer = "/till/" + number + "/set/open";
                            KasseState[numberKasse].state = "open";
                        }
                        break;
                    case "close":
                    case "CLOSE":
                        if (numberway == 1)
                        {
                         
                        }
                        else
                        {
                            answer = "/till/" + number + "/set/open";
                            KasseState[numberKasse].state = "open";
                        }
                        break;
                    case "closing":
                    case "CLOSING":
                        if (numberway == 1)
                        {
                            
                        }
                        else
                        {
                            answer = "/till/" + number + "/set/close";
                            KasseState[numberKasse].state = "close";
                        }
                        break;      
                    default:
                        break;
                }

                System.Console.WriteLine($" Answer state-<{answer}>");
                byte[] data = Encoding.UTF8.GetBytes(answer);
                int numberOfSentBytes = client.Send(data, data.Length);

                for (int i = 0; i < int.Parse(txtBTill.Text); i++)
                {

                }
            }
            else
                MessageBox.Show("Could not create connection.", "Error Connection", MessageBoxButton.OK, MessageBoxImage.Warning);
            return answer;
        }
        public string Send_state_Kasse(int numberKasse)
        {
            string answer = "unknown";
            string number = "0" + numberKasse;
            numberKasse -= 1;
            numberway = cmbBoxWay.SelectedIndex;



            if ((connect_state))
            {
                /* KasseState.KsAr[numberKasse] - its current state  
                 *  numberway :
                 * 1 = short
                 * 2 = long
                 * **/
                if (((int.Parse(txtBThTill.Text)-1) != numberKasse) || (KasseCurStateFunction.BREAK != true))
                {
                    switch (KasseState[numberKasse].state)
                    {
                        case "open":
                        case "OPEN":
                            if (numberway == 1)
                            {
                                answer = "/till/" + number + "/set/close";
                                KasseState[numberKasse].state = "close";
                            }
                            else
                            {
                                answer = "/till/" + number + "/set/closing";
                                KasseState[numberKasse].state = "closing";
                            }
                            break;
                        case "opening":
                        case "OPENING":
                            if (numberway == 1)
                            {
                                answer = "/till/" + number + "/set/open";
                                KasseState[numberKasse].state = "open";
                            }
                            else
                            {
                                answer = "/till/" + number + "/set/close";
                                KasseState[numberKasse].state = "close";
                            }
                            break;
                        case "close":
                        case "CLOSE":
                            if (numberway == 1)
                            {
                                answer = "/till/" + number + "/set/open";
                                KasseState[numberKasse].state = "open";
                            }
                            else
                            {
                                answer = "/till/" + number + "/set/opening";
                                KasseState[numberKasse].state = "opening";
                            }
                            break;
                        case "closing":
                        case "CLOSING":
                            if (numberway == 1)
                            {
                                answer = "/till/" + number + "/set/close";
                                KasseState[numberKasse].state = "close";
                            }
                            else
                            {
                                answer = "/till/" + number + "/set/opening";
                                KasseState[numberKasse].state = "close";
                            }
                            break;
                        case "BREAK":
                        case "break":
                            answer = "/till/" + number + "/set/BREAK";
                            KasseState[numberKasse].state = "BREAK";
                            break;
                        case "MANAGER":
                        case "manager":
                            answer = "/till/" + number + "/set/MANAGER";
                            break;
                        case "CALLAC":
                        case "callac":
                            answer = "/till/" + number + "/set/CALLAC";
                            break;
                        default:
                            break;
                    }

                    System.Console.WriteLine($" Answer state-<{answer}>");
                    byte[] data = Encoding.UTF8.GetBytes(answer);
                    int numberOfSentBytes = client.Send(data, data.Length);

                    for (int i = 0; i < int.Parse(txtBTill.Text); i++)
                    {

                    }
                }
            }
            else
            {
                if (!connect_state)
                    MessageBox.Show("Could not create connection.", "Error Connection", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            return answer;
        }
        private void BtnKasse1_Click(object sender, EventArgs e)
        {
            Send_state_Kasse(1);
        }
        private void BtnKasse2_Click(object sender, EventArgs e)
        {
            Send_state_Kasse(2);
        }
        private void BtnKasse3_Click(object sender, EventArgs e)
        {
            Send_state_Kasse(3);
        }
        private void BtnKasse4_Click(object sender, EventArgs e)
        {
            Send_state_Kasse(4);
        }
        private void BtnKasse5_Click(object sender, EventArgs e)
        {
            Send_state_Kasse(5);
        }
        private void BtnKasse6_Click(object sender, EventArgs e)
        {
            Send_state_Kasse(6);
        }
        private void BtnKasse7_Click(object sender, EventArgs e)
        {
            Send_state_Kasse(7);
        }
        private void BtnKasse8_Click(object sender, EventArgs e)
        {
            Send_state_Kasse(8);
        }

        private void defaultInstance_WindowClosed(object sender, EventArgs e)
        {
            client.Close();
        }
        public string Send_Get_State()
        {
            string answer = "unknown";

            answer = "/till/#"; //fix 1 bug

            if (connect_state)
            {
                System.Console.WriteLine($" Answer state-<{answer}>");
                byte[] data = Encoding.UTF8.GetBytes(answer);
                int numberOfSentBytes = client.Send(data, data.Length);

                for (int i = 0; i < int.Parse(txtBTill.Text); i++)
                {

                }
            }

            return answer;
        }
        public string Send_option_Kasse(int numberTill, string state)
        {
            string answer = "unknown";
            string number = "0" + numberTill;

            answer = "/till/" + number + "/set/" + state;

            if (connect_state)
            {
                System.Console.WriteLine($" Answer send-<{answer}>");
                byte[] data = Encoding.UTF8.GetBytes(answer);
                int numberOfSentBytes = client.Send(data, data.Length);

                for (int i = 0; i < int.Parse(txtBTill.Text); i++)
                {

                }
            }

            return answer;
        }
        private void BtnOk_Click(object sender, EventArgs e)
        {
            Send_OK(int.Parse(txtBThTill.Text));
            /*
             * Sascha Dt O, [09.02.20 14:29]
                OK =

                Sascha Dt O, [09.02.20 14:29]
                If status = opening then we send :

                Sascha Dt O, [09.02.20 14:29]
                /till/03/set/open

                Sascha Dt O, [09.02.20 14:29]
                if status = open or closing then we send:

                Sascha Dt O, [09.02.20 14:30]
                /till/03/set/close
             */
            //Send_option_Kasse(int.Parse(txtBThTill.Text), "OK");
        }

        private void BtnBreak_Click(object sender, EventArgs e)
        {

            if (KasseCurStateFunction.BREAK == true)
            {
                Send_option_Kasse(int.Parse(txtBThTill.Text), "break/off");
                KasseCurStateFunction.BREAK = false;
                
            }
            else
            {
                Send_option_Kasse(int.Parse(txtBThTill.Text), "break/on");
                KasseCurStateFunction.BREAK = true;
                
            }
        }

        private void BtnML_Click(object sender, EventArgs e)/*CALLAC*/
        {
            if (KasseCurStateFunction.CALLAC == true)
            {
                Send_option_Kasse(int.Parse(txtBThTill.Text), "callac/off");
                KasseCurStateFunction.CALLAC = false;
            }
            else
            {
                Send_option_Kasse(int.Parse(txtBThTill.Text), "callac/on");
                KasseCurStateFunction.CALLAC = true;
            }
        }

        private void BtnMA_Click(object sender, EventArgs e)/*MANAGER*/
        {
            if (KasseCurStateFunction.MANAGER == true)
            {
                Send_option_Kasse(int.Parse(txtBThTill.Text), "manager/off"); //Same with MA (Callac)
                KasseCurStateFunction.MANAGER = false;
            }
            else
            {
                Send_option_Kasse(int.Parse(txtBThTill.Text), "manager/on");
                KasseCurStateFunction.MANAGER = true;
            }
        }

        private void BtnSnooze_Click(object sender, EventArgs e)
        {
            Send_option_Kasse(int.Parse(txtBThTill.Text), "snooze");
            KasseCurStateFunction.ALARM = false;
        }

        private void BtnAlarm_Click(object sender, EventArgs e)
        {

        }

        private void BtnClose_Click(object sender, EventArgs e)
        {
            connect_state = false;
            client.Close();
            Application.Current.Shutdown();
        }
        static bool CheckIp(string address) //check for correct IP addres
        {
            var nums = address.Split('.');
            int useless;
            return nums.Length == 4 && nums.All(n => int.TryParse(n, out useless)) &&
               nums.Select(int.Parse).All(n => n < 256);
        }
        private void TxtBIP_KeyPress(object sender, TextCompositionEventArgs e)
        {
            TextBox tbne = sender as TextBox;
            if ((!Char.IsDigit(e.Text, 0)) && (e.Text != "."))
            {
                { e.Handled = true; }
            }
            else
                if ((e.Text == ",") && ((tbne.Text.IndexOf(",") != -1) || (tbne.Text == "")))
            { e.Handled = true; }
           
        }
            private bool IsNumber(string Text)
            {
                int output;
                return int.TryParse(Text, out output);
            }    

        private void TxtBPort_KeyPress(object sender, TextCompositionEventArgs e)
        {
            if (IsNumber(e.Text) == false)   // check for enter only numeral
                e.Handled = true;
        }

        private void TxtBTill_KeyPress(object sender, TextCompositionEventArgs e)
        {
            if (IsNumber(e.Text) == false)   // check for enter only numeral
                e.Handled = true;
        }

        private void TxtBThTill_KeyPress(object sender, TextCompositionEventArgs e)
        {
            if (IsNumber(e.Text) == false)   // check for enter only numeral
                e.Handled = true;
        }
        private void Window_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }
        private void BtnSave_Click(object sender, EventArgs e)
        {
            string path = System.IO.Path.GetDirectoryName(System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName) + "\\net.ini";            
            INIManager inimanager = new INIManager(path);
            /* for reconnect to new NET param - need to reboot programm (need fix) */

            if (CheckIp(txtBIP.Text))
            {
                inimanager.WritePrivateString("net", "IP", txtBIP.Text);
                inimanager.WritePrivateString("net", "Port", txtBPort.Text);
                inimanager.WritePrivateString("net", "Tills", txtBTill.Text);
                inimanager.WritePrivateString("net", "NTill", txtBThTill.Text);
                inimanager.WritePrivateString("net", "way", cmbBoxWay.Text);
                inimanager.WritePrivateString("net", "PortList", txtBPort_Listen.Text); 
                createConnect();
            }
            else                
            MessageBox.Show("Wrong IP", "NET Param", MessageBoxButton.OK, MessageBoxImage.Warning);


        }
        private void TxtBPort_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void ManagerviewToolStripMenuItem_hm(object sender, MouseButtonEventArgs e)
        {
            for (int i = 0; i < 8; i++)
                btns[i].Visibility = Visibility.Hidden;

            for (int i = 0; i < int.Parse(txtBTill.Text); i++)
                btns[i].Visibility = Visibility.Visible;

            groupBoxConfig.Visibility = Visibility.Visible;
            DockPanelManager.Visibility = Visibility.Hidden;
        }

        private void ConfigurationToolStripMenuItem_hm(object sender, MouseButtonEventArgs e)
        {
            for (int i = 0; i < Convert.ToInt32(txtBTill.Text); i++)
                btns[i].Visibility = Visibility.Visible;
            numberway = cmbBoxWay.SelectedIndex;
            if (numberway == 1)/*short way no. Not hidden. Only the letters ok should be away*/
                BtnOk.Content = "";
            else
                BtnOk.Content = "OK";

            
            currentThill = int.Parse(txtBThTill.Text);

            groupBoxConfig.Visibility = Visibility.Hidden;
            DockPanelManager.Visibility = Visibility.Visible;
        }

        private void ManagerviewToolStripMenuItem_hm(object sender, KeyEventArgs e)
        {

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            
        }
    }
}
