
Q:
1. if we used "Long", we will have never open/close (change state) "Kasse X" from application? Only from remote control?

/till/01/set/open   -> Short way 
/till/01/set/opening -> long way 

/till/01/set/close   -> Short way 
/till/01/set/closing -> long way 

Long way Logic is like this: 


If till is closed
First push 

.../opening

Button Start Green flashing

Second push
.../close

Become Red

If till is open
First push 

.../closing

Button Start Red flashing

Second push
.../open

Become Green

Second Color from flashing is White. 
Green flashing means: 1 sec white with Black border / 1 sec green

Red flashing means: 1 sec white with Black border / 1 sec red
--------------------------------------------------------------------------------------------------------------
Oleh -, [01.12.19 16:42]
Alex, good day.
I need you to explain that:
1)
"When you get xxbreak you have to make the button in first row Brown. xx is till (button) number. When somebody push this button you have to send /till/xx/set/break."  
push this - its mean button in first row number xx?
2)
"When you get xxCALLAC in second row the lila button starts flashing. Look to screeny. When somebody push this Button you send /till/xx/set/CALLAC "
push this - (question like the first) its button in second row "MA" ?
 When i need disable flashing button?
3)
"When you get in answear xxMANAGER orange Button in second starts flashing. When somebody push this Button you send /till/xx/set/MANAGER"
What is the button push this .
 When i need disable flashing button?
4)
"OK Button: When somebody push ok and this till is closed you send /till/xx/set/open    
If this till status is open you send /till/xx/set/closed
"
Can you make example by 2 buttons.
5)
"till/# must be send after every button press and every x seconds. To have everytime actuall Status."
When i press button KasseXX - i send  (example) "till/01/set/open". 
Do i have to send some message for state all button? like 
"till/01open/02close/03opening/03CALLAC/04closing/05close/05MANAGER/05CALLAC/06close/07closing/08close/"

 P.S. I made testing transmit and receive data between application and remote device(smartphone). Now I need to finished some logic (has many state KasseXX in one moment XXopen/XXMANAGER/XXBREAK/XXCALLAC).

Sascha Dt O, [01.12.19 18:28]
Hi Oleg

Sascha Dt O, [01.12.19 18:39]
1) No means somebody push button Break in second row. Every software have it own till number. For example we say in config your software is 01. then you send till/01/set/break

Sascha Dt O, [01.12.19 18:40]
2) WHen CallAC is away from status or somebody push it in your software

Sascha Dt O, [01.12.19 18:40]
3) Same like in Answear 2

Sascha Dt O, [01.12.19 18:42]
4) You are just a virtual tableau for system. You have 1 ok button. This button works only for til lyou are in config. For example 01. Then you send till/01/set/open

Sascha Dt O, [01.12.19 18:43]
5) till/# is just be up to date. With this you get same answer like on event you sent. Actuall Status from system. It is just to show everytime correct status for every till.

Sascha Dt O, [01.12.19 18:43]
No you dont have to send something like this.

Sascha Dt O, [01.12.19 18:44]
PS: Sounds good

Oleh -, [01.12.19 19:07]
o, about "This Till". You wrote -  "This till define what till number we have" - its like number  kasse (not number button Kasse "Tills") ?

Oleh -, [01.12.19 19:08]
what do you mean by "This till"

Sascha Dt O, [01.12.19 19:14]
This till: Defines what till is this Software. when there is staying 01 all your buttons in second row have 01 in command.

Sascha Dt O, [01.12.19 19:14]
If there is staying 02 all second row Buttons have 02

Oleh -, [01.12.19 19:15]
ok, i get it


   <hamburgerMenu:HamburgerMenu Background="#FFFEE801" MenuIconColor="Black" SelectionIndicatorColor="Black" MenuItemForeground="White" HorizontalAlignment="Left">
                    <hamburgerMenu:HamburgerMenuItem Text="Configuration" SelectionCommand="{Binding MyCommand}" />
                    <hamburgerMenu:HamburgerMenuItem Text="Managerview" />
                </hamburgerMenu:HamburgerMenu>